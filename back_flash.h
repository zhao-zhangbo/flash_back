///******************************************************************************
// * @brief    flash备份机制驱动模块(CRC32校验依赖https://github.com/whik/crc-lib-c.git)
// * @note  C99语法 用户的p_data需在尾部预留4字节，用于驱动模块进行数据校验
// * Change Logs:
// * Date           Author         Email                   Notes
// * 
// ******************************************************************************/

#ifndef __BACK_FLASH_H__
#define __BACK_FLASH_H__

#include "stdint.h"
#include "string.h"
#include "crclib.h"

#ifdef _BACK_FLASH_GLOBALS_
#define BACK_FLASH_EXTERN  
#else
#define BACK_FLASH_EXTERN extern
#endif 

#ifndef B_SUCCESS
#define B_SUCCESS 0
#define B_ERROR 1
#endif


//flash读函数原型
typedef uint8_t flash_read_cb_t(uint32_t address,void *buffer, uint32_t len);
//flash写函数原型
typedef uint8_t flash_write_cb_t(uint32_t address,void *buffer, uint32_t len);

//flash类数据结构
typedef struct{
	const char *pname;
    uint8_t* p_data;
    uint32_t data_len;
    flash_read_cb_t *flash_read_cb;
    flash_write_cb_t *flash_write_cb;
    uint32_t flash_address_main;
    uint32_t flash_address_back;
}back_flash_type;

extern uint8_t back_flash_write(back_flash_type *back_flash_t);
extern uint8_t back_flash_read(back_flash_type *back_flash_t);

BACK_FLASH_EXTERN back_flash_type back_flash_config;

#endif
























