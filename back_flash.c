///******************************************************************************
// * @brief    flash备份机制驱动模块(CRC32校验依赖https://github.com/whik/crc-lib-c.git)
// * @note  用户的p_data需在尾部预留4字节，用于驱动模块进行数据校验
// * @note  用户需实现带保护的读写函数
// * Change Logs:
// * Date           Author         Email                   Notes
// * 2023-12-21      子恒        3393018959@qq.com      init 
// ******************************************************************************/

#define _BACK_FLASH_GLOBALS_
#include "back_flash.h"

uint8_t back_flash_write(back_flash_type *back_flash_t)
{
    uint32_t crc;
    uint8_t buf[back_flash_t->data_len];

    memcpy(buf,back_flash_t->p_data,back_flash_t->data_len);
    crc = crc32(buf,back_flash_t->data_len - 4);
    *((uint32_t *)(&buf[back_flash_t->data_len - 4])) = crc;

    if(back_flash_t->flash_write_cb == NULL) return B_ERROR;
    
    if(back_flash_t->flash_write_cb(back_flash_t->flash_address_main, buf, back_flash_t->data_len) != B_SUCCESS) return B_ERROR;

    if(back_flash_t->flash_write_cb(back_flash_t->flash_address_back, buf, back_flash_t->data_len) != B_SUCCESS) return B_ERROR; 
}


uint8_t back_flash_read(back_flash_type *back_flash_t)
{
    uint32_t crc,crctmp;
    uint8_t buf[back_flash_t->data_len];
    uint8_t i;
    uint8_t chaeck_pass = 0;

    if(back_flash_t->data_len <= 4) return B_ERROR;
    if(back_flash_t->flash_write_cb == NULL) return B_ERROR;
    if(back_flash_t->flash_read_cb == NULL) return B_ERROR;

    if(back_flash_t->flash_read_cb(back_flash_t->flash_address_main,buf,back_flash_t->data_len) != B_SUCCESS) return B_ERROR;

    crc = crc32(buf,back_flash_t->data_len - 4);
    crctmp = *((uint32_t *)(&buf[back_flash_t->data_len - 4]));

    //主存储区数据校验通过
    if(crc == crctmp){
        memcpy(back_flash_t->p_data,buf,back_flash_t->data_len);
        chaeck_pass = 1;
    }

    if(back_flash_t->flash_read_cb(back_flash_t->flash_address_back,buf,back_flash_t->data_len) != B_SUCCESS) return B_ERROR;

    crc = crc32(buf,back_flash_t->data_len - 4);
    crctmp = *((uint32_t *)(&buf[back_flash_t->data_len - 4]));

    //备份区数据校验通过,主存储区数据校验未通过，则以备份区数据为准，并重写主存储区
    if( (crc == crctmp) && (chaeck_pass != 1) ){
        //printf("备份区数据校验通过,主存储区数据校验未通过，则以备份区数据为准，并重写主存储区\r\n");
        memcpy(back_flash_t->p_data,buf,back_flash_t->data_len);
        if(back_flash_t->flash_write_cb(back_flash_t->flash_address_main, back_flash_t->p_data, back_flash_t->data_len) != B_SUCCESS) return B_ERROR;
        return B_SUCCESS;
    }
    //如果主存储区数据校验通过，备份区数据校验不通过，则重写备份区
    if( (crc != crctmp) && (chaeck_pass == 1) ){
        //printf("如果主存储区数据校验通过，备份区数据校验不通过，则重写备份区\r\n");
        if(back_flash_t->flash_write_cb(back_flash_t->flash_address_back, back_flash_t->p_data, back_flash_t->data_len) != B_SUCCESS) return B_ERROR;
        return B_SUCCESS;
    }
    if(chaeck_pass != 1) return B_ERROR;
    return B_SUCCESS;
}





























